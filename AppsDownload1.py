from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import pandas as pd
driver = webdriver.Chrome(ChromeDriverManager().install())
url = "https://app.sensortower.com/ios/rankings/top/iphone/us/all-categories?date=2022-02-20"
driver.get(url)

records =[]
cells = driver.find_element(By.CSS_SELECTOR,".app-cell dl-rev-expanded")
for cell in cells:
    Appname = driver.find_element(By.CSS_SELECTOR,".name").text.strip()
    publisher_name = driver.find_element(By.CSS_SELECTOR,".rankings-publisher-name").text.strip()
    ratings_stars = driver.find_element(By.CSS_SELECTOR,".ratings-stars").text.strip()
    ratings = driver.find_element(By.CSS_SELECTOR,".ratings").text.strip()
    downloads = driver.find_element(By.CSS_SELECTOR,".total-downloads").text.strip()
    records.append([Appname, publisher_name, ratings_stars, ratings,downloads])

df = pd.DataFrame(records, columns=['Appname', 'publisher_name', 'ratings_stars', 'ratings','downloads'])
df

# records =[]
# driver.get("https://www.price.com.hk/product.php?p=529218")
#
# cells = driver.find_element_by_class_name(".item-inner")
#
# for cell in cells:
#     Price = driver.find_element_by_class_name(".data-price")
#     Merchant = driver.find_element_by_class_name(".quotation-merchant-name")
#     Merchant = driver.find_element_by_class_name(".header-bottom. Line-01 > .column-01 img").get_attribute("src")
#
# records.append([Price,Merchant])
# driver.find_element_by_css_selector()
# driver.find_element(By.CSS_Selector,"body")



